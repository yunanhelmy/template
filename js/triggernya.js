$(document).ready(function(){
	// custom validator
	$.validator.addMethod("alphabet", function(value, element) {
	    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
	});

	var attributnya = {
		rules: {
		    phone: {
		      required: true,
		      number: true
		    },
		    hobby: {
		    	required: true,
		    	alphabet: true
		    }
		 },
		 messages: {
		 	hobby: {
		    	alphabet: "hanya boleh alfabet"
		    }
		 }
	}

	$("#form2").validate(attributnya);
	$("#form3").validate(attributnya);
});